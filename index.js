const _ = require('lodash')
const buildLink = require('./buildLink.js')
const sort = require('./sort.js')
const host = require('./host.js')

module.exports = function(args){
    if(!_.isObject(args)){
        throw new Error('Data was expected in instance in an object.')
    }

    if(!_.isObject(args.data)){
        throw new Error('Data was expected in a "data" property in object.')
    }  

    var self = this;
    self.data = args.data;
    self.buildLink = buildLink;
    self.sort = sort;    
    if(typeof args.host === 'string'){
        self.host = args.host;
    }
    else{
        if(host.isLocal()){
            self.host = host.default;
        }
        else{
            self.host = host.getBasePath();
        }
    }

    if(args.sort !== false){
        self.data = self.sort({
            data: args.data,
            host: self.host
        });
    }

    return self;
}